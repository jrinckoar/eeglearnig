"""
eegLearninTools


"""


import numpy as np
import scipy.io
from scipy.fftpack import fft, ifft
from scipy.signal import hilbert
from matplotlib import pyplot as plt


def loadEEGdata(file, headerFile):
    data = scipy.io.loadmat(file)
    info = scipy.io.loadmat(headerFile)
    data = data["data"]
    header = info["header"]
    header_chanlocs = header["chanlocs"][0][0]
    chanlocs = []
    for i in header_chanlocs:
        chanlocs.append(i[0][0][0][0])

    return data, chanlocs


def simSelect(lTask, m):

    if m == 1:
        mText = "M01"
    elif m == 2:
        mText = "M02"
    elif m == 3:
        mText = "M03"
    else:
        mText = ""

    if lTask == "BASELINE":
        ltText = "LT01"
    elif lTask == "ADAPTATION":
        ltText = "LT02"
    elif lTask == "RETENTION":
        ltText = "LT03"
    else:
        ltText = ""
    return ltText, mText


def loadSimData(path, simID, lTask, m, nsur):

    ltText, mText = simSelect(lTask, m)
    file = path + simID + "_" + ltText + "_" + mText + ".mat"
    # load Data
    data_ = scipy.io.loadmat(file, simplify_cells=True, squeeze_me=True)
    simopt = data_["opt"]
    data = data_["data"]

    preFile = path + "surrogates/" + simID + "s" + "_" + ltText + "_" + mText + "__"
    txys, tyxs = loadSimSurrogates(preFile, nsur)

    data["txys"] = txys
    data["tyxs"] = tyxs
    return data, simopt


def loadSimSurrogates(preFile, nsur):

    # load simopt
    file = preFile + "000.mat"
    data_ = scipy.io.loadmat(file, simplify_cells=True, squeeze_me=True)
    dataShape = data_["data"]["txys"].shape
    # simopt = data_["opt"]
    # nsur = sim_opt["nsur"]
    txys = np.zeros(((nsur,) + dataShape))
    tyxs = np.zeros(((nsur,) + dataShape))

    for i in range(nsur):

        if i < 10:
            surText = "00{0}.mat"
        elif 10 <= i < 100:
            surText = "0{0}.mat"
        else:
            surText = "{0}.mat"

        file = preFile + surText.format(i)
        data_ = scipy.io.loadmat(file, simplify_cells=True, squeeze_me=True)
        txys[i, :, :, :] = data_["data"]["txys"]
        tyxs[i, :, :, :] = data_["data"]["tyxs"]

    return txys, tyxs


def simPythonToMatlab(opath, path, simID, lTask, m, nsur):

    data, simopt = loadSimData(path, simID, lTask, m, nsur)
    simopt['nsur'] = nsur
    ofile = opath + 'dataSim{0}_{1}_M0{2}.mat'.format(simID,lTask,m)
    sim = {**data, **simopt}
    print(sim.keys())
    scipy.io.savemat(ofile, sim)


def surrogate(data):
    '''
        Generate surrogate data
    '''
    X = np.abs(fft(data, axis=1))
    # random phase
    randPhase = np.angle(fft(np.random.randn(*data.shape), axis=1))
    xs = np.real(ifft(X * np.exp(1j * randPhase), axis=1))
    xs = (xs - xs.mean(axis=1, keepdims=True)) / xs.std(axis=1, keepdims=True)
    return np.angle(hilbert(xs, axis=1))



if __name__ == "__main__":
    # path = "/home/jrestrepo/Datos/eegLearning/"
    # # fname = "RBH nf filt epochs resampled ADAPTATION clean 40hz pruned with ICA.mat"
    # fname = "RBH nf filt epochs resampled BASELINE clean 40hz pruned with ICA_MAT.mat"
    # data, chanlocs = loadMatFile(path + fname, path + "info2.mat")
    # print(chanlocs)
    # plt.plot(data[0, 3, :])
    # plt.show()

    simConf = {
        "opath": "/home/jrestrepo/Datos/eegLearning/sims/sim01/",
        "path": "/home/jrestrepo/Datos/eegLearning/sims/sim01/",
        "simID": "PP01",
        "lTask": "BASELINE",
        "m": 3,
        "nsur": 16,
    }
    simPythonToMatlab(**simConf)

    # data, simopt = loadSimData(**sim)
    # print(data["txys"].shape)
    # print(simopt["ch_inds"].shape)

# def statAnalysis(simData, sLevel):
#     txy = simData["txy"]
#     tyx = simData["tyx"]
#     txys = simData["txys"]
#     tyxs = simData["tyxs"]
#     sLevel = sLevel / 2

#     nsur, epochs, nrea, U = simData["txys"].shape
#     
#     plt.hist(np.squeeze(txy[:, :, 0]), 32)
#     
#     pVal = np.zeros((epochs, nrea, U))
#     T = np.zeros((epochs, nrea, U))

#     # for u in range(U):
#     #     tx = np.squeeze(txy[:, :, u])
#     #     ty = np.squeeze(tyx[:, :, u])

#     #     txs = np.squeeze(txys[:, :, :, u])
#     #     tys = np.squeeze(tyxs[:, :, :, u])

#     #     # Center data
#     #     mx = txs.mean(axis=0, keepdims=True)
#     #     my = tys.mean(axis=0, keepdims=True)

#     #     tx = tx - mx
#     #     ty = ty - my
#     #     txs = txs - mx
#     #     tys = tys - my

#     #     d = tx - ty
#     #     ds = txs - tys
#     #     ms = ds.mean(axis=0)
#     #     t = np.sort(np.vstack((d, ds)), axis=0)
#     #     ind = np.argmax(t == d, axis=0)
#     #     r = np.zeros((epochs, nrea))
#     #     r[np.squeeze(d <= ms)] = ind[np.squeeze(d <= ms)]
#     #     r[np.squeeze(d > ms)] = nsur - ind[np.squeeze(d > ms)]
#     #     p = (r + 1) / (nsur + 1)

#     #     pVal[p <= sLevel, u] = p[p <= sLevel]
#     #     h = np.squeeze(d) - ms
#     #     T[p <= sLevel, u] = h[p <= sLevel]

#     return T, pVal

# def getEmpiricalCDF(data, nbins, sLevel):
#     nrea = data.shape[1]
#     cdf = np.zeros((nbins, nrea))
#     bin_edges = np.zeros((nbins, nrea))
#     bounds = np.zeros((2, nrea))

#     for i in range(nrea):
#         h, x = np.histogram(data[:, i], bins=nbins, density=True)
#         cdf_ = np.cumsum(h) * (x[1] - x[0])
#         il = np.argmax(cdf_ >= sLevel)
#         iu = np.argmax(cdf_ >= 1 - sLevel)
#         bounds[0, i] = x[il]
#         bounds[1, i] = x[iu]
#         cdf[:, i] = cdf_
#         bin_edges[:, i] = x[:-1]
#     return cdf, bin_edges, bounds

# def statAnalysis02(simData, sLevel):
#     txy = simData["txy"]
#     tyx = simData["tyx"]
#     txys = simData["txys"]
#     tyxs = simData["tyxs"]
#     sLevel = sLevel / 2
#     nsur, epochs, nrea, U = simData["txys"].shape


#     txys = txys.reshape((nsur * epochs, nrea, U))
#     tyxs = tyxs.reshape((nsur * epochs, nrea, U))

#     plt.hist(np.squeeze(txy[:, :, 0]), 32)

#     pVal = np.zeros((epochs, nrea, U))
#     T = np.zeros((epochs, nrea, U))

#     # for u in range(U):
#     #     tx = np.squeeze(txy[:, :, u])
#     #     ty = np.squeeze(tyx[:, :, u])

#     #     txs = np.squeeze(txys[:, :, u])
#     #     tys = np.squeeze(tyxs[:, :, u])

#     #     # Center data
#     #     mx = txs.mean(axis=0, keepdims=True)
#     #     my = tys.mean(axis=0, keepdims=True)

#     #     tx = tx - mx
#     #     ty = ty - my
#     #     txs = txs - mx
#     #     tys = tys - my
#     #     ds = np.sort(txs - tys, axis=0)
#     #     ms = ds.mean(axis=0)
#     #     cdf, bin_edges, bounds = getEmpiricalCDF(ds, 128, sLevel)

#     #     for i in range(epochs):
#     #         d = tx[i, :] - ty[i, :]
#     #         h = np.zeros(*d.shape)
#     #         h[(d <= ms)] = d[(d <= ms)] - ds[0, (d <= ms)]
#     #         h[(d > ms)] = d[(d > ms)] - ds[-1, (d > ms)]
#     #         indl = d < bounds[0, :]
#     #         indu = d > bounds[1, :]
#     #         T[i, indl, u] = h[indl]
#     #         T[i, indu, u] = h[indu]

#     #         # t = np.sort(np.vstack((d, ds)), axis=0)
#     #         # ind = np.argmax(t == d, axis=0)
#     #         # r = np.zeros(*ind.shape)
#     #         # r[np.squeeze(d <= ms)] = ind[np.squeeze(d <= ms)]
#     #         # r[np.squeeze(d > ms)] = nsur * epochs - ind[np.squeeze(d > ms)]
#     #         # p = (r + 1) / (nsur * epochs + 1)

#     #         # pVal[i, p <= sLevel, u] = p[p <= sLevel]
#     #         # h = np.zeros(*d.shape)
#     #         # h[(d <= ms)] = d[(d <= ms)] - ds[0, (d <= ms)]
#     #         # h[(d > ms)] = d[(d > ms)] - ds[-1, (d > ms)]

#     #         # T[i, p <= sLevel, u] = h[p <= sLevel]

#     return T, pVal
