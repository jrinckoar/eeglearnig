#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
EEG learning task transfer entropy

sim01 surrogates: phase-phase


'Juan Felipe Restrepo <jrestrepo@ingenieria.uner.edu.ar>'
2021-09-13
===============================================================================
===============================================================================
[SimConfig]
Sim_filename='PP01s'
Sim_variables={'LT':[1, 2, 3], 'M':[3, 4]}
Sim_realizations={'SREA':1}
Sim_name='PP01s'
Sim_hostname='cluster-fiuner'
Sim_MatExecutable='matlab2018'
[endSimConfig]
[SlurmConfig]
#SBATCH --mail-user=jrestrepo@ingenieria.uner.edu.ar
#SBATCH --partition=internos
#SBATCH --nodes=1
#SBATCH --ntasks=24
#SBATCH --tasks-per-node=24
[endSlurmConfig]
"""
import sys
import os
import numpy as np
import psutil
import ray
import random
import socket
import itertools
from scipy.signal import hilbert
from scipy.io import savemat
from datetime import datetime
from knnIMpy.knnTools import getAlpha
from knnIMpy.knnTent import knnTent01 as tf

try:
    sys.path.append(
        os.path.join(os.path.dirname(__file__), "../../../../", "eegLtools")
    )
    import eegLtools as eegT
except ImportError:
    sys.path.append(os.path.join(os.path.dirname(__file__), "../../../", "eegLtools"))
    import eegLtools as eegT


if not ray.is_initialized():
    num_cpus = psutil.cpu_count(logical=False)
    # ray.init(num_cpus=num_cpus)
    ray.init(num_cpus=6)  # algo raro con el cluster


@ray.remote
def calc_tent(indSorce, indTarget, eeg, eegs, tf_opt):
    return tf(eegs[indSorce, :], eeg[indTarget, :], **tf_opt)


# Simulation parameters
LT = 1
M = 1
SREA = 1
NSUR = 120
RINDEX = 0
# ----------------------------------------------------------------------------
# Change random seed
random.seed()
seed = random.randint(1, 5000)
np.random.seed(seed)
print("seed = {0}".format(seed), flush=True)

# output file
path = os.getcwd()
OUTDIR = path + "/outData/"
if not os.path.isdir(OUTDIR):
    os.makedirs(OUTDIR)
    print("created folder : ", OUTDIR, flush=True)
else:
    print(OUTDIR, "folder already exists.", flush=True)
pre_oname = "PP01s_LT0{0}_M0{1}".format(LT, M)


# host settings
hostname = socket.gethostname()
if hostname == "apolo":
    # Apolo
    dataPath = "/home/jrestrepo/Datos/eegLearning/Data/"
else:
    # neptuno
    dataPath = "/home/jrestrepo/eeglearning/Data/"
sim_opt = {}
sim = {}
sim_opt["seed"] = seed
sim_opt["hostname"] = hostname
# ----------------------------------------------------------------------------
# Simulation
now = datetime.now()
dt_string_ini = now.strftime("%d/%m/%Y %H:%M:%S")
sim_opt["simdate"] = dt_string_ini
print("------------------------", flush=True)
print("simulation init  " + dt_string_ini, flush=True)
print("------------------------", flush=True)
# ----------------------------------------------------------------------------
# Data
LearningTask = ""
if LT == 1:
    LearningTask = "BASELINE"
elif LT == 2:
    LearningTask = "ADAPTATION"
elif LT == 3:
    LearningTask = "RETENTION"

dataFile = "RBH fb filt epochs resampled {0} clean 40hz pruned with ICA int_MAT.mat"
dataFile = dataFile.format(LearningTask)
dataFile = dataPath + dataFile

headerFile = dataPath + "info2.mat"
data, chanlocs = eegT.loadEEGdata(dataFile, headerFile)
# average the epochs
data = np.squeeze(data.mean(axis=0))
nch, n = data.shape

# channel indices
i = []
j = []
for ch_inds in itertools.combinations(range(nch), 2):
    i.append(ch_inds[0])
    j.append(ch_inds[1])
ch_inds = (i, j)
nrea = int(nch * (nch - 1) / 2)

sim_opt["ch_inds"] = ch_inds
sim_opt["chanlocs"] = chanlocs
sim_opt["fs"] = 40
sim_opt["nch"] = nch
sim_opt["nrea"] = nrea
sim_opt["dataLength"] = n
sim_opt["learningTask"] = LearningTask
# ----------------------------------------------------------------------------
# Get phase
# Normalize cero mean unitary variance
data = (data - data.mean(axis=1, keepdims=True)) / data.std(axis=1, keepdims=True)
phase = np.angle(hilbert(data, axis=1))
print("phases done", flush=True)
# ----------------------------------------------------------------------------
# Transfer entropy parameters
m = M
tau = 5
u = np.array([1, 3, 5, 10, 15, 25])
u = u + m
U = u.size
kNearestN = 25
# alpha = getAlpha(kNearestN, 3 * m)
sim_opt["m"] = m
sim_opt["tau"] = tau
sim_opt["kNearestN"] = kNearestN
# sim_opt["alpha"] = alpha
sim_opt["snorm"] = False
sim_opt["vcor"] = False
sim_opt["nsur"] = NSUR
print("alpha done", flush=True)
# ----------------------------------------------------------------------------
# Transfer entropy
now = datetime.now()
dt_string_ini = now.strftime("%d/%m/%Y %H:%M:%S")
sim_opt["simdate"] = dt_string_ini
print("------------------------", flush=True)
print("simulation init  " + dt_string_ini, flush=True)
print("------------------------", flush=True)
# ----------------------------------------------------------------------------
# Transfer entropy
# set algorithm parameters
tf_opt = {
    "m": m,
    "tau": tau,
    "u": 1,
    "nn": kNearestN,
    # "alpha": alpha,
    "snorm": False,
    "vcor": False,
}
eeg_id = ray.put(np.squeeze(phase))

for r in range(RINDEX, NSUR):
    now = datetime.now()
    local_dt_string_ini = now.strftime("%d/%m/%Y %H:%M:%S")

    # outputfile
    oname = pre_oname
    if r < 10:
        oname = oname + "_00{0}.mat"
    elif 10 <= r < 100:
        oname = oname + "_0{0}.mat"
    else:
        oname = oname + "_{0}.mat"
    oname = oname.format(r)
    print(
        "........................\nSurrogate {0}/{1}\n{2} {3}".format(
            r + 1, NSUR, oname, local_dt_string_ini
        ),
        flush=True,
    )

    # generate surrogate phases
    phaseS = eegT.surrogate(data)
    eegs_id = ray.put(np.squeeze(phaseS))

    txys = np.zeros((nrea, U))
    tyxs = np.zeros((nrea, U))
    for k in range(U):
        tf_opt["u"] = u[k]
        Txy_ray_ids = []
        Tyx_ray_ids = []
        for i, j in zip(*ch_inds):
            Txy_ray_ids.append(calc_tent.remote(i, j, eeg_id, eegs_id, tf_opt))
            Tyx_ray_ids.append(calc_tent.remote(j, i, eeg_id, eegs_id, tf_opt))
        txys[:, k] = np.array(ray.get(Txy_ray_ids))
        tyxs[:, k] = np.array(ray.get(Tyx_ray_ids))

        print("{0:.2f} %".format((k + 1) / U * 100), flush=True)

    sim["data"] = {"txys": txys, "tyxs": tyxs}
    sim["opt"] = sim_opt
    savemat(OUTDIR + oname, sim)
    now = datetime.now()
    dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
    print(dt_string, flush=True)

print("------------------------", flush=True)
print("Sim start : ", dt_string_ini, flush=True)
now = datetime.now()
dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
print("Sim end: ", dt_string, flush=True)
print("------------------------", flush=True)
